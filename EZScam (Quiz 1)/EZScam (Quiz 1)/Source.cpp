#include <string>
#include <iostream>

using namespace std;

void sortScam(int scam[]);
int suggestScam(int scam[], int gold, int itemPrice);
void scammingScript(int scam[], int& gold, int itemPrice);

int main()
{
	int scamList[7] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int playerGold = 250;
	int itemPrice;
	int suggestedscam;
	string iProgramToScam;

	sortScam(scamList);

	while (true)
	{
		system("cls");
		cout << "Current Gold: " << playerGold;
		cout << endl;
		cout << endl;

		cout << "EPIC STORE!!!" << endl;
		for (int i = 0; i < 7; i++)
		{
			cout << "Package #" << i + 1 << " " << scamList[i] << endl;
		}

		cout << "Package #7 is our BEST VALUE!!!" << endl;
		cout << endl;
		cout << "The price of the item that the player would like to buy: ";
		cin >> itemPrice;

		if (playerGold >= itemPrice)
		{
			playerGold -= itemPrice;
		}
		else
		{
			scammingScript(scamList, playerGold, itemPrice);
			cout << endl;
			//flavour text
			cout << "Thank you for your purchase!";
			cout << "Type your credit card number here and," << endl;
			cout << "if you purchase Package 7 in the future you get a 20% off discount + free 200000 In-Game Currency" << endl;
			cout << "Credit Card Number: ";
			cin >> iProgramToScam;
		}

		system("pause");
	}
}

//sorting the scam
void sortScam(int scam[])
{
	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 6 - i; j++)
		{
			if (scam[j] > scam[j + 1])
			{
				scam[j] += scam[j + 1];
				scam[j + 1] = scam[j] - scam[j + 1];
				scam[j] -= scam[j + 1];
			}
		}
	}
}

//suggest the scam                                        package 7 has the BEST VALUE
int suggestScam(int scam[], int gold, int itemPrice)
{
	for (int i = 0; i < 7; i++)
	{
		if ((scam[i] + gold - itemPrice) > 0)
		{
			return i + 1;
		}
	}

	return 0;
}

void scammingScript(int scam[], int& gold, int itemPrice)
{
	char playerChoice;
	system("cls");
	cout << "It seems you do not have enough money to buy that item" << endl;
	cout << "Would you like to buy one of our packages? (y/n): ";
	cin >> playerChoice;
	if (playerChoice == 'y')
	{
		if (suggestScam(scam, gold, itemPrice) == 0)
		{
			cout << "We are very sorry but we cannot offer a Package right now that covers that price" << endl;
		}
		else
		{
			cout << "We recommend Package #7, its our B E S T  D EA L" << endl;

			//even though we want to scam the player into buying package 7, saying it again will look dumb
			if (suggestScam(scam, gold, itemPrice) != 7)
			{
				cout << "however if you are only looking for a Package that covers that amount, we would recommend" << endl;
				cout << "Package #" << suggestScam(scam, gold, itemPrice) << endl;
			}

			cout << "Would you like to buy Package #" << suggestScam(scam, gold, itemPrice) << "? (y/n) ";
			cin >> playerChoice;

			if (playerChoice == 'y')
			{
				gold += scam[suggestScam(scam, gold, itemPrice) - 1] - itemPrice;
			}
		}
	}
}