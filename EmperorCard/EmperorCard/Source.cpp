#include <string>
#include <iostream>
#include <vector>
#include <time.h>

using namespace std;

void setWhiteSide(vector<string>& cardSet);
void setBlackSide(vector<string>& cardSet);
void displayCards(vector<string> cardSet);
void distributeCitizens(vector<string>& vector);
int dealerCard(vector<string> cardSet);
int decideWinner(vector<string> player, int playerChoice, vector<string> dealer, int dealerChoice, int& prizeMultiplier);
void resetCards(vector<string>& playerSide, vector<string>& dealerSide, bool emperorSide);
void setPlayerMoney(int& playerMoney, int prizeMultiplier, int playerBet);

int main()
{
	vector<string>playerSide;
	vector<string>dealerSide;

	srand(time(NULL));

	int playerChoice = 0;
	int dealerChoice = 0;
	int playerLife = 30;
	int playerBet;
	int prizeMultiplier = 0;
	bool hasEmperorCard;
	int playerMoney = 0;

	while (playerChoice > 2 || playerChoice < 1) // choose side
	{
		system("cls");

		cout << "Choose a side: " << endl << endl;
		cout << "[1] Emperor" << endl;
		cout << "[2] Slave" << endl;
		cin >> playerChoice;

		if (playerChoice > 2 || playerChoice < 1)
		{
			cout << "Please choose either [1] or [2]" << endl;
			system("pause");
		}
	}

	if (playerChoice == 1) //sets the cards you chose
	{
		setWhiteSide(playerSide);
		setBlackSide(dealerSide);
		hasEmperorCard = true;
	}
	else
	{
		setWhiteSide(dealerSide);
		setBlackSide(playerSide);
		hasEmperorCard = false;
	}

	cout << "You have chosen the " << playerSide[0] << " side first" << endl;
	cout << "The game begins" << endl;
	system("pause");

	for (int i = 0; i < 12; i++)
	{
		if (playerLife <= 0) //if player dies
		{
			break;
		}

		if (playerMoney < 0) //so that money does not go to negative
		{
			playerMoney = 0;
		}

		if (i % 3 == 0 && i > 0)
		{
			//playerSide.swap(dealerSide); //does the old switcharoo
			if (hasEmperorCard == true)
			{
				setWhiteSide(dealerSide);
				setBlackSide(playerSide);
				hasEmperorCard = false;
			}
			else
			{
				setWhiteSide(playerSide);
				setBlackSide(dealerSide);
				hasEmperorCard = true;
			}
		}

		playerBet = 0;
		while (playerBet > playerLife || playerBet <= 0) //limits the bet amount
		{
			system("cls");
			cout << "Round: " << i + 1 << endl;
			cout << "Milimeters left: " << playerLife << endl;
			cout << "Side: " << playerSide[0] << endl;
			cout << "Money Earned: " << playerMoney << endl;
			cout << "Enter Bet (" << playerLife << " max): ";

			cin >> playerBet;
			if (playerBet > playerLife || playerBet <= 0)
			{
				cout << "Bet is invalid" << endl;
				system("pause");
			}
		}

		playerChoice = 0;
		while (playerChoice != 1 || dealerChoice != 0) //loop until the emepror or slave card is picked
		{
			dealerChoice = rand() % dealerSide.size() + 0;
			cout << "The Dealer has pciked a card, what card will you play?" << endl;

			displayCards(playerSide);

			cin >> playerChoice;
			system("cls");
			cout << "You both turn your cards over" << endl;
			cout << "The dealer played the " << dealerSide[dealerChoice] << " card" << endl;
			cout << "You played the " << playerSide[playerChoice - 1] << " card" << endl;

			switch (decideWinner(playerSide, playerChoice, dealerSide, dealerChoice, prizeMultiplier))
			{
			case 1:
				cout << "You win, you gained " << playerBet * 100000 * prizeMultiplier << " in this round" << endl;
				setPlayerMoney(playerMoney, prizeMultiplier, playerBet);
				break;

			case 2:
				cout << "Tie, the game continues" << endl;
				break;

			case 3:
				cout << "You lost, and the drill goes deeper in your ear" << endl;
				playerLife -= playerBet;
				break;
			}

			cout << endl;
			system("pause");

			if (playerChoice == 1 || dealerChoice == 0) // if special cards reset the cards and start a new round
			{
				resetCards(playerSide, dealerSide, hasEmperorCard);
				break;
			}
			else
			{
				playerSide.erase(playerSide.begin() + playerChoice - 1);
				dealerSide.erase(dealerSide.begin() + dealerChoice);
			}
		}
	}

	if (playerMoney >= 20000000 && playerLife > 0) //best win condition
	{
		cout << "You won the game" << endl;
		cout << "ep" << endl;
		cout << "zz" << endl;
	}
	else if (playerLife > 0) //meh win condition
	{
		cout << "You won the game but you didnt reach your goal" << endl;
	}
	else // l |li|
		 // ll|L
	{
		cout << "You lost..." << endl;
	}

	system("pause");
}

void setPlayerMoney(int& playerMoney, int prizeMultiplier, int playerBet) //sets player money
{
	playerMoney += playerBet * 100000 * prizeMultiplier;
}

void resetCards(vector<string> & playerSide, vector<string> & dealerSide, bool emperorSide) //for card reset
{
	if (emperorSide == true)
	{
		setWhiteSide(playerSide);
		setBlackSide(dealerSide);
	}
	else
	{
		setWhiteSide(dealerSide);
		setBlackSide(playerSide);
	}
}

int decideWinner(vector<string> player, int playerChoice, vector<string> dealer, int dealerChoice, int& prizeMultiplier)
{
	playerChoice = playerChoice - 1;

	if (player[playerChoice] == "Emperor")
	{
		if (dealer[dealerChoice] == "Slave")
		{
			prizeMultiplier = 0;

			return 3;
		}
		else if (dealer[dealerChoice] == "Emperor")
		{
			prizeMultiplier = 0;

			return 2;
		}
		else
		{
			prizeMultiplier = 1;

			return 1;
		}
	}
	else if (player[playerChoice] == "Slave")
	{
		if (dealer[dealerChoice] == "Emperor")
		{
			prizeMultiplier = 5;

			return 1;
		}
		else if (dealer[dealerChoice] == "Slave")
		{
			prizeMultiplier = 0;

			return 2;
		}
		else
		{
			prizeMultiplier = 0;

			return 3;
		}
	}
	else
	{
		if (dealer[dealerChoice] == "Emperor")
		{
			prizeMultiplier = 0;

			return 3;
		}
		else if (dealer[dealerChoice] == "Citizen")
		{
			prizeMultiplier = 0;

			return 2;
		}
		else
		{
			prizeMultiplier = 1;

			return 1;
		}
	}
}

int dealerCard(vector<string> cardSet) //sets dealers card
{
	int number = rand() % cardSet.size() + 1;
	return number;
}

void setBlackSide(vector<string> & cardSet)
{
	cardSet.clear();
	cardSet.push_back("Slave");
	distributeCitizens(cardSet);
}

void setWhiteSide(vector<string> & cardSet)
{
	cardSet.clear();
	cardSet.push_back("Emperor");
	distributeCitizens(cardSet);
}

void distributeCitizens(vector<string> & cardSet)
{
	for (int i = 0; i < 4; i++)
	{
		cardSet.push_back("Citizen");
	}
}

void displayCards(vector<string> cardSet)
{
	for (int i = 0; i < cardSet.size(); i++)
	{
		cout << "[" << i + 1 << "]" << cardSet[i] << endl;
	}
}