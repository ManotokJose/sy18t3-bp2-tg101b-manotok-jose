#include <string>
#include <iostream>
#include "Wizards.h"
#include "Spell.h"


using namespace std;


int main()
{
	string name;

	Wizards* playerWizard = new Wizards;
	Wizards* enemyWizard = new Wizards;

	Spell* fireBall = new Spell;
	Spell* energyBall = new Spell;

	energyBall->createEnergyBall();
	fireBall->createFireBall();

	cout << "Player Name: ";
	cin >> name;
	playerWizard->assignStats(name);

	system("cls");

	cout << "Enemy Name: ";
	cin >> name;
	enemyWizard->assignStats(name);

	cout << "They fight" << endl;
	system("pause");

	while (playerWizard->health > 0 && playerWizard->mana > 0 && enemyWizard->health > 0 && enemyWizard->mana > 0)
	{
		playerWizard->wizardAttack(enemyWizard, fireBall);
		enemyWizard->wizardAttack(playerWizard, energyBall);

		cout << playerWizard->name << ": " << playerWizard->health << endl;
		cout << enemyWizard->name << ": " << enemyWizard->health << endl;

		system("pause");
	}

	delete playerWizard;
	delete enemyWizard;
	delete fireBall;
	delete energyBall;
	return 0;
}

