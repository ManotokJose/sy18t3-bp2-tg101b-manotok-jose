#pragma once
#include <string>

using namespace std;
class Spell
{
public:
	Spell();
	~Spell();
	
	string name;

	int damage;
	int manaCost;

	void createFireBall();
	void createEnergyBall();
};

