#include "Wizards.h"
#include "Spell.h"
#include <iostream>

void Wizards::assignStats(string name)
{
	this->name = name;
	this->mana = 100;
	this->health = 100;
}

void Wizards::wizardAttack(Wizards* defense, Spell* magic)
{
	defense->health -= magic->damage;
	this->mana -= magic->manaCost;
}


Wizards::Wizards()
{
}


Wizards::~Wizards()
{
}
