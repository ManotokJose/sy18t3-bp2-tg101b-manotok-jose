#pragma once
#include <string>
#include "Spell.h"

using namespace std;

class Wizards
{
public:
	Wizards();
	~Wizards();

	string name;

	int health;
	int mana;

	void assignStats(string name);
	void wizardAttack(Wizards* defense, Spell* magic);
};

