#include <string>
#include <iostream>
#include <time.h>

using namespace std;

int rollDice();
void betGold(int& gold, int& bet);
void payOut(int& gold, int& bet);
void playRound(int& gold);
//int otherFunction(int gold);

int main()
{
	srand(time(NULL));

	int playerGold = 1000;

	while (playerGold > 0)
	{
		playRound(playerGold);
	}
	system("pause");
}

void playRound(int& gold)
{
	int playerBet;

	cout << "Gold: " << gold << endl;
	cout << "Bet: ";
	cin >> playerBet;

	if (playerBet > 0 && playerBet <= gold)
	{
		betGold(gold, playerBet);
		payOut(gold, playerBet);
	}
}

void betGold(int& gold, int& bet)
{
	gold -= bet;
}

void payOut(int& gold, int& bet)
{
	int dealerDice = rollDice();
	int playerDice = rollDice();
	int betMultiplier = 2;

	cout << "Dealer total dice value: " << dealerDice << endl;
	cout << "Player total dice value: " << playerDice << endl;

	if (dealerDice == 2 || playerDice == 2)
	{
		int betMultiplier = 3;
	}

	if (dealerDice > playerDice)
	{
		cout << "Player Loses" << endl;
		gold -= bet * betMultiplier;
	}
	else if (dealerDice < playerDice)
	{
		cout << "Player Wins" << endl;
		gold += bet * betMultiplier;
	}
	else if (dealerDice == playerDice)
	{
		cout << "Tie" << endl;
		gold += bet;
	}

}

/*int otherFunction(int& gold)
{
gold -= bet;
return bet;
}*/

int rollDice()
{
	return rand() % 12 + 2;
}