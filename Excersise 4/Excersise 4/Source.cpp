#include <string>
#include <iostream>
#include <time.h>
#include <conio.h>

using namespace std;

int generateItem(int& gold, string& name);
void enterDungeon(int& playerGold, string itemName, int itemGold);

int main()
{
	srand(time(NULL));

	struct lootVariables
	{
		string Name;
		int Gold;

	}item;

	int playerGold = 50;

	while (true)
	{
		if (playerGold < 25)
		{
			cout << "You lose";
			system("cls");
			system("pause");
			break;
		}
		else if (playerGold >= 500)
		{
			cout << "You win";
			system("cls");
			system("pause");
			break;
		}
		else
		{
			cout << "Do you want to enter the dungeon? Press any key, you have no choice";
			_getch();
			system("cls");
			enterDungeon(playerGold, item.Name, item.Gold);
		}
	}

}

int generateItem(int& gold, string& name)
{
	int itemValue[5] = { 100, 50, 25, 5, 0 };
	string itemName[5] = { "Mithril Ore", "Sharp Talon", "Thick Leather", "Jellopy", "Cursed Stone" };
	int itemID = rand() % 5 + 0;

	gold = itemValue[itemID];
	name = itemName[itemID];
	return itemID;
}

void enterDungeon(int& playerGold, string itemName, int itemGold)
{
	int dungeonGold = 0;
	int goldMultiplier = 1;
	char playerChoice;
	int itemID;

	playerGold -= 25;
	cout << "Deducted 25 gold from player" << endl;
	cout << "Entering the dungeon" << endl;

	while (true)
	{
		itemID = generateItem(itemGold, itemName);

		//for player experience
		while (itemID == 4 && goldMultiplier == 1)
		{

			itemID = generateItem(itemGold, itemName);

		}


		if (itemID == 4)
		{
			cout << "You found, " << itemName << endl;
			cout << "You feel a shroud of darkness go over you, and you begin to lose your memory" << endl;
			goldMultiplier = 1;
			dungeonGold = 0;
			system("pause");
			system("cls");
			break;

		}
		else
		{
			cout << "Level: " << goldMultiplier << endl;
			cout << "Stored Gold: " << playerGold << endl;
			cout << "You found, " << itemName << "!" << endl;
			cout << itemName << " costs: " << itemGold << endl;

			dungeonGold += itemGold * goldMultiplier;

			cout << "Gold Aquired: " << dungeonGold << endl;
			cout << "Do you wish to continue? 'n' to exit";
			cin >> playerChoice;

			if (playerChoice == 'n' || playerChoice == 'N')
			{
				playerGold += dungeonGold;
				goldMultiplier = 1;
				dungeonGold = 0;
				break;
			}
			else
			{
				goldMultiplier++;
			}
		}
		system("cls");
	}
}