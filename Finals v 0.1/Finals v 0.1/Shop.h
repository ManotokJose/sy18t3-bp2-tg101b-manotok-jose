#pragma once
#include <string>
#include <vector>
#include "Player.h"
#include "Armour.h"
#include "Weapon.h"

using namespace std;

class Shop
{
public:
	Shop();
	~Shop();

	vector<Weapon*> shopWeapon;
	vector<Armour*> shopArmour;

	Player* shopCustomer;

	void displayWares();
	bool checkForMoney();

private:
	int randomNumber(int max, int min);
};

