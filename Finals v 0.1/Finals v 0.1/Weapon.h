#pragma once
#include <string>
#include <iostream>

using namespace std;

class Weapon
{
public:
	Weapon(bool offHand);
	~Weapon();

	string weaponName;
	string weaponElement;
	string weaponType;
	string weaponPrefix;
	string weaponSpecialElement;

	bool onlyTwoHanded;

	int weaponDamage;
	//int armourPen;
	int weaponPrice;
	int damageNegation;
	//int critChance;

private:

	int randomNumber(int max, int min);
	void setValues(bool twoHanded, int damage, int damNeg, int crit);
	
};

