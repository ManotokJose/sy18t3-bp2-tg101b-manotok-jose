#pragma once
#include <string>

using namespace std;

class Ability
{
public:
	Ability();
	~Ability();

	string abilityName;
	
	int abilityDamage;
	int abilityCost;
};

