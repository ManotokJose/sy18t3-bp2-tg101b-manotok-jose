#pragma once
#include <string>
#include "Job.h"

using namespace std;

class Character : public Job
{
public:
	Character();
	~Character();

	class Weapons;
	class Armour;
	class Ability;
	class Job;

	string characterName;

	int characterMove;
	int characterSpeed;
	int characterJump;
	int characterMaxHealth;
	int characterHealth;
	int characterMaxMagicPoints;
	int charcterMagicPoints;
	int characterCT;
	int characterAT;
	int characterCEV;
	int characterSEV;
	int characterAEV;
	int characterBrave;
	int characterFaith;
	int characterLevel;
	int characterExperience;

	bool isFemale;
	bool isMagicBased;

	Weapons* rightHand;
	Weapons* leftHand;
	Armour* armourHelmet;
	Armour* armourChestPlate;
	Armour* armourCharm;

	Job* currentJob;

	Ability* playerAbiliy[5];

	void addAbility(Job* ability);
};

