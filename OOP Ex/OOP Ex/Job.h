#pragma once
#include <string>

using namespace std;

class Job
{
public:
	Job();
	~Job();

	class Ability;

	int jobLevel;
	int jobExp;
	int jobPromo;
	int jobNext;

	string jobName;

	Ability* jobAbility;
};

