#pragma once
#include <string>

using namespace std;

class Weapons
{
public:
	Weapons();
	~Weapons();

	string weaponName;
	string weaponType;

	int weaponMPCost;
	int weaponPower;

	bool isRanged;
	bool isMagicRelated;
	bool canBlock;
};

