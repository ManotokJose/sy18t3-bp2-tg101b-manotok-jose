#pragma once
#ifndef NODE_H
#define NODE_H

#include <string>
#include <iostream>
#include <time.h>

using namespace std;

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;
};

class circularList
{
public:

	Node* head = NULL;
	Node* temporary = NULL;

	void createNewGuard(string victimName)
	{
		//at the start of the function, make a new node
		Node* wallGuard = new Node;

		//set the .name of the new node
		wallGuard->name = victimName;

		if (head == NULL)
		{
			//sets first node
			head = wallGuard;
			wallGuard->next = head;
		}
		else
		{
			//sets next nodes
			wallGuard->next = head->next;
			head->next = wallGuard;
			head = wallGuard;
		}
	}

	void display(int playerNumber)
	{
		Node* temp;
		int randomNumber = rand() % 100 + 1;

		for (int i = 0; i < playerNumber; i++)
		{
			//sets up .previous for the nodes
			temp = head;
			head = head->next;
			head->previous = temp;
		}

		//"randomizes" list
		for (int i = 0; i < randomNumber; i++)
		{
			head = head->next;
		}

		//checks if there is only one node
		if (head->next == head)
		{
			cout << head->name << " will go to seek for reinforcements";
		}
		//else
		else
		{
			//display list of nodes
			for (int i = 0; i < playerNumber - 1; i++)
			{
				cout << "Guard " << head->name << endl;
				head = head->next;
			}
			cout << "Guard " << head->name << endl;
		}
		cout << endl;
	}

	void passCloth(int playerNumber)
	{
		//"dice roll"
		int randomNumber = rand() % playerNumber + 1;

		//sets the temporary node
		head = head->next;
		temporary = head;

		cout << head->name << " drew a " << randomNumber << endl;
		for (int i = 0; i < randomNumber; i++)
		{
			temporary = temporary->next;
		}
		cout << temporary->name << " will be eliminated" << endl;
		system("pause");
		deleteGuard();
	}

	void deleteGuard()
	{
		//cuts the pointers that lead to temporary node, and point it to the other following nodes
		head = temporary->previous;
		head->next = temporary->next;
		temporary->next->previous = head;
		delete temporary;
	}
}list;
#endif //NODE_H