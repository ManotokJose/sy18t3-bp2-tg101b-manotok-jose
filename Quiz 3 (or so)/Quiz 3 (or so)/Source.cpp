#include <string>
#include <iostream>
#include <time.h>
#include "Node.h"

using namespace std;

int main()
{
	int numberOfSoldiers;
	string soldierName;

	srand(time(NULL));

	cout << "You need to assign people to the Night's Watch, how many people would you like to assign? ";
	cin >> numberOfSoldiers;

	for (int i = 0; i < numberOfSoldiers; i++)
	{
		cout << "Guard #" << i + 1 << " : ";
		cin >> soldierName;
		list.createNewGuard(soldierName);
	}

	while (numberOfSoldiers != 0)
	{
		system("cls");
		list.display(numberOfSoldiers);
		if (numberOfSoldiers > 1)
		{
			list.passCloth(numberOfSoldiers);
		}
		numberOfSoldiers--;
	}
	cout << endl;
	system("pause");
}

