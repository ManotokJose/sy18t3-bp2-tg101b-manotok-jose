#include <string>
#include <iostream>
#include <time.h>

using namespace std;

class Monsters
{
private:

	string Name;
	int Atk;
	int Def;
	int Spd;
	int Level;

public:

	int Health;

	void createMonster()
	{
		cout << "Name: ";
		cin >> Name;

		cout << "Level: ";
		cin >> Level;

		cout << "Base Health: ";
		cin >> Health;
		Health = Health * 1.5 * Level;

		cout << "Base Attack: ";
		cin >> Atk;
		Atk = Atk * 1.5 * Level;

		/*cout << "Base Def: ";
		cin >> Def;
		Def = Def * 1.5 * Level;

		cout << "Base Speed: ";
		cin >> Spd;
		Spd = Spd * 1.5 * Level;
		system("cls");*/
	}

	void displayStats()
	{
		cout << "Monster Name: " << Name << endl;
		cout << "Health: " << Health << endl;
		cout << "Attack Power: " << Atk << endl;
	/*	cout << "Defense: " << Def << endl;
		cout << "Speed: " << Spd << endl;*/
	}

	void monsterAttack(Monsters * enemy)
	{
		enemy->Health -= this->Atk;
	}

	
};

void displayMonstersStats(Monsters * monsterJuan, Monsters * monster2);

int main()
{
	Monsters* monsterJuan = new Monsters;
	Monsters* monsterTwo = new Monsters;

	cout << "Monster 1" << endl;
	monsterJuan->createMonster();

	cout << "Monster 2" << endl;
	monsterTwo->createMonster();

	monsterFight(monsterJuan, monsterTwo);

	system("pause");
}

void displayMonstersStats(Monsters * monsterJuan, Monsters * monster2)
{
	monsterJuan->displayStats();
	cout << endl;
	monster2->displayStats();
}

void monsterFight(Monsters* juan, Monsters* two)
{
	while (juan->Health > 0 || two->Health > 0)
	{
		juan->monsterAttack(two);
		displayMonstersStats(juan, two);
		two->monsterAttack(juan);
		displayMonstersStats(juan, two);
	}
}